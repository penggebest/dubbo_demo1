# dubbo_demo1

#### 项目介绍
dubbo 的一个入门学习的例子
ss 

> Apache Dubbo Apache Dubbo™ (incubating)是一款高性能Java RPC框架。下午重新整理一下，纪录下来

1、eclipse 建立一个maven项目

![屏幕快照 2018-09-02 下午8.33.50.png](https://ask.qcloudimg.com/draft/597711/xqr4efiwqt.png)

```
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>dubbolangtutu</groupId>
	<artifactId>dubbolangtutu</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>dubbolangtutu</name>
	<url>http://maven.apache.org</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>dubbo</artifactId>
			<version>2.6.1</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.zookeeper</groupId>
			<artifactId>zookeeper</artifactId>
			<version>3.4.6</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-log4j12</artifactId>
				</exclusion>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
  <groupId>org.apache.curator</groupId>
  <artifactId>curator-client</artifactId>
  <version>2.11.0</version>
</dependency>

<dependency>
  <groupId>org.apache.curator</groupId>
  <artifactId>curator-framework</artifactId>
  <version>2.11.0</version>
</dependency>
	</dependencies>
</project>
```

2、建立接口和对应实现类

GreetingService 接口

```
package dubbolangtutu.dubbolangtutu.service;

public interface GreetingService {
    public String sayHello(String name);
}
```

GreetingServiceImpl 实现类

```
package dubbolangtutu.dubbolangtutu.service;

public class GreetingServiceImpl implements GreetingService {


	public String sayHello(String name) {
        return "Hello " + name;
    }

}
```

2、新建生产者

```
package dubbolangtutu.dubbolangtutu;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.ServiceConfig;

import dubbolangtutu.dubbolangtutu.service.GreetingService;
import dubbolangtutu.dubbolangtutu.service.GreetingServiceImpl;

import java.io.IOException;
 
public class Provider {

    public static void main(String[] args) throws IOException {
        ServiceConfig<GreetingService> serviceConfig = new ServiceConfig<GreetingService>();
        serviceConfig.setApplication(new ApplicationConfig("first-dubbo-provider"));
        serviceConfig.setRegistry(registry());
        serviceConfig.setInterface(GreetingService.class);
        serviceConfig.setRef(new GreetingServiceImpl());
        serviceConfig.export();
        System.in.read();
    }
    public static RegistryConfig registry() {
    	          RegistryConfig registryConfig = new RegistryConfig();
    	          registryConfig.setAddress("127.0.0.1:2181");
    	          registryConfig.setProtocol("zookeeper");
    	          return registryConfig;
         }
}
```

3、新建消费者

```
package dubbolangtutu.dubbolangtutu;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;

import dubbolangtutu.dubbolangtutu.service.GreetingService;

public class Consumer {
    public static void main(String[] args) {
        ReferenceConfig<GreetingService> referenceConfig = new ReferenceConfig<GreetingService>();
        referenceConfig.setApplication(new ApplicationConfig("first-dubbo-consumer"));
        referenceConfig.setRegistry(new RegistryConfig("zookeeper://127.0.0.1:2181"));
        referenceConfig.setInterface(GreetingService.class);
        GreetingService greetingService = referenceConfig.get();
        System.out.println(greetingService.sayHello("world"));
    }
}
```

4、启动zookeeper

![屏幕快照 2018-09-02 下午8.36.51.png](https://ask.qcloudimg.com/draft/597711/3gz4mb610x.png)

5、启动生产者 ，然后启动消费者 查看输出

消费者启动：

```
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
Hello world
```

附件：

[https://gitee.com/penggebest/dubbo\_demo1](https://gitee.com/penggebest/dubbo_demo1)