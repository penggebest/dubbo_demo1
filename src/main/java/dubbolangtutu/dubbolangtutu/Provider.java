package dubbolangtutu.dubbolangtutu;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.ServiceConfig;

import dubbolangtutu.dubbolangtutu.service.GreetingService;
import dubbolangtutu.dubbolangtutu.service.GreetingServiceImpl;

import java.io.IOException;
 
public class Provider {

    public static void main(String[] args) throws IOException {
        ServiceConfig<GreetingService> serviceConfig = new ServiceConfig<GreetingService>();
        serviceConfig.setApplication(new ApplicationConfig("first-dubbo-provider"));
        serviceConfig.setRegistry(registry());
        serviceConfig.setInterface(GreetingService.class);
        serviceConfig.setRef(new GreetingServiceImpl());
        serviceConfig.export();
        System.in.read();
    }
    public static RegistryConfig registry() {
    	          RegistryConfig registryConfig = new RegistryConfig();
    	          registryConfig.setAddress("127.0.0.1:2181");
    	          registryConfig.setProtocol("zookeeper");
    	          return registryConfig;
         }
}
